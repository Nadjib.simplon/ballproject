// Create Scene
let x = 25;
let y = 12;
let x2,y2,z,z2,z3
const root = document.getElementById('root');
root.style.display = 'flex';
root.style.justifyContent = 'center';
root.style.alignItems = 'center';
const screen = document.createElement('div');
screen.style.position = "relative";
screen.style.width = "700px";
screen.style.overflow = 'hidden'
screen.style.height = "400px";
screen.style.border = "solid 1px black";
screen.style.background = "linear-gradient(to bottom, #94c5f8 1%,#a6e6ff 70%,#b1b5ea 100%)";

const floor = document.createElement('div');
floor.style.position = "absolute";
floor.style.bottom = 0;
floor.style.width = "700px";
floor.style.height = "10px";
floor.style.border = "solid 1px green";
floor.style.backgroundColor = "green";
screen.appendChild(floor);

const box = document.createElement('div');
box.style.position = "absolute";
box.style.bottom = "12px";
box.style.right = "25px";
box.style.width = "80px";
box.style.height = "120px";
box.style.backgroundColor = "#eae060";
box.style.border = "solid 2px #bbb44f";
box.style.borderRadius = "5px";
screen.appendChild(box);

const perso = document.createElement('div');
perso.id = "perso";
perso.style.position = "absolute";
perso.style.width = "30px";
perso.style.transition = ".6s";
perso.style.height = "30px";
perso.style.backgroundColor = "red";
perso.style.border = "solid 2px black";
perso.style.borderRadius = "20px";
screen.appendChild(perso);

root.appendChild(screen);

class movement {
    constructor(addToX,jump) {
        x = x + addToX

        if(x < 563 || x > 671)root.style.setProperty('--y', 12 + 'px')

        console.log(x)
        if (jump) {

            perso.classList.add('jump')
            setTimeout(() => {
            }, 500)

            perso.classList.add('fall')

            setTimeout(() => {
                if(x >= 550) {
                    root.style.setProperty('--y', 130 + 'px')
                }else{
                    root.style.setProperty('--y', 12 + 'px')
                }
                perso.classList.remove('jump')
                perso.classList.remove('fall')

            }, 500)

        } else
            root.style.setProperty('--x', x + 'px')
    }

}

document.addEventListener('keydown', (event) => {
    switch (event.key) {
        case 'ArrowUp':
            new movement(1, true);
            break;
        case 'ArrowLeft':
            new movement(-18, 0);
            x2 = -25
            break;
        case 'ArrowRight':
            new movement(18, 0);
            x2 = 25
            break;
    }
});

